#!/bin/bash
# Basic shell commands to give system status
echo "Kernel info"
uname -a
echo "Directory info"
df -h
echo "Linux Release info"
cat /etc/lsb-release
cat /etc/redhat-release
echo "Files in root (/)"
ls -lh /
echo "Memory and Swap Info"
free -h
